import json

from django.conf import settings
from django.http import JsonResponse
import requests

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication

from apps.order.models import Order


@api_view(['POST'])
@authentication_classes([JWTAuthentication])
@permission_classes([IsAuthenticated])
def place_order(request):
    headers = {
        'Content-Type': 'application/json',
    }
    payload = json.dumps(request.data)
    response = requests.request("POST", settings.SHOPIFY_SHOP_ORDER_URL, headers=headers, data=payload)
    response_json = response.json()
    order_json = response_json.get('order')
    for v in ['admin_graphql_api_id', 'checkout_id', 'confirmed', 'contact_email', 'device_id', 'gateway', 'landing_site_ref', 'reference', 'source_identifier', 'source_url', 'total_price_usd']:
        del order_json[v]
    Order.objects.update_or_create(
            id=order_json.get('id'),
            defaults=order_json
        )
    resp = JsonResponse(response_json, safe=False, status=status.HTTP_200_OK)
    return resp
