from django.db import models

# Create your models here.

CANCEL_REASON_CHOICES_LIST = ['customer', 'fraud', 'inventory', 'declined', 'other']

CANCEL_REASON_CHOICES = [(e, e) for e in CANCEL_REASON_CHOICES_LIST]

CANCEL_REASON_CHOICES_MAP = {e: e for e in CANCEL_REASON_CHOICES_LIST}

FS_CHOICES_LIST = ['pending', 'authorized', 'partially_paid',
                   'paid', 'partially_refunded', 'refunded', 'voided']

FS_CHOICES = [(e, e) for e in FS_CHOICES_LIST]

FS_CHOICES_MAP = {e: e for e in FS_CHOICES_LIST}

FFS_CHOICES_LIST = ['fulfilled', 'null', 'partial', 'restocked']

FFS_CHOICES = [(e, e) for e in FFS_CHOICES_LIST]

FFS_CHOICES_MAP = {e: e for e in FFS_CHOICES_LIST}

PM_CHOICES_LIST = ['checkout', 'direct', 'manual', 'offsite', 'express', 'free']

PM_CHOICES = [(e, e) for e in PM_CHOICES_LIST]

PM_CHOICES_MAP = {e: e for e in PM_CHOICES_LIST}


class Order(models.Model):

    app_id = models.PositiveBigIntegerField()

    browser_ip = models.GenericIPAddressField(null=True)

    buyer_accepts_marketing = models.BooleanField(default=False)

    cancel_reason = models.CharField(choices=CANCEL_REASON_CHOICES, default='customer', max_length=20, null=True)

    cancelled_at = models.DateTimeField(null=True)

    cart_token = models.UUIDField(null=True)

    checkout_token = models.UUIDField(null=True)

    client_details = models.JSONField(blank=True, null=True)

    closed_at = models.DateTimeField(null=True)

    created_at = models.DateTimeField()

    currency = models.CharField(max_length=3)

    current_total_duties_set = models.JSONField(blank=True, null=True)

    customer_locale = models.CharField(max_length=8, null=True)

    discount_applications = models.JSONField(blank=True, null=True)

    discount_codes = models.JSONField(blank=True, null=True)

    email = models.EmailField()

    financial_status = models.CharField(choices=FS_CHOICES, max_length=20)

    fulfillments = models.JSONField(blank=True, null=True)

    fulfillment_status = models.CharField(choices=FFS_CHOICES, max_length=20, null=True)

    id = models.PositiveBigIntegerField(primary_key=True)

    landing_site = models.URLField(null=True)

    line_items = models.JSONField(blank=True)

    location_id = models.CharField(max_length=64, blank=True, null=True, default=None)

    name = models.CharField(max_length=100)

    note = models.CharField(max_length=255, blank=True, null=True)

    note_attributes = models.JSONField(null=True, blank=True)

    number = models.PositiveIntegerField()

    order_number = models.PositiveIntegerField()

    order_status_url = models.URLField()

    original_total_duties_set = models.JSONField(null=True, blank=True)

    payment_gateway_names = models.JSONField(null=True, blank=True)

    phone = models.CharField(max_length=13, null=True, blank=True)

    presentment_currency = models.CharField(max_length=3)

    processed_at = models.DateTimeField()

    processing_method = models.CharField(choices=PM_CHOICES, max_length=20, default='checkout')

    referring_site = models.URLField(null=True)

    refunds = models.JSONField(null=True, blank=True)

    shipping_lines = models.JSONField(null=True, blank=True)

    source_name = models.CharField(max_length=20)

    subtotal_price = models.DecimalField(max_digits=10, decimal_places=2)

    subtotal_price_set = models.JSONField(blank=True, null=True)

    tags = models.CharField(max_length=64000, blank=True, null=True)

    tax_lines = models.JSONField(blank=True, null=True)

    taxes_included = models.BooleanField()

    test = models.BooleanField()

    token = models.UUIDField()

    total_discounts = models.DecimalField(max_digits=10, decimal_places=2)

    total_discounts_set = models.JSONField(blank=True, null=True)

    total_line_items_price = models.DecimalField(max_digits=10, decimal_places=2)

    total_line_items_price_set = models.JSONField(blank=True, null=True)

    total_price = models.DecimalField(max_digits=10, decimal_places=2)

    total_price_set = models.JSONField(blank=True, null=True)

    total_shipping_price_set = models.JSONField(blank=True, null=True)

    total_tax = models.DecimalField(max_digits=10, decimal_places=2)

    total_tax_set = models.JSONField(blank=True, null=True)

    total_tip_received = models.DecimalField(max_digits=10, decimal_places=2)

    total_weight = models.PositiveIntegerField()

    updated_at = models.DateTimeField()

    user_id = models.CharField(max_length=20, null=True, blank=True)

