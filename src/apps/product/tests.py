import json

from django.contrib.auth.models import User
from django.test import TestCase, client

# Create your tests here.
from django.utils.timezone import now
from rest_framework import status
from rest_framework.reverse import reverse_lazy, reverse
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import SlidingToken

from apps.product.models import Product
from apps.product.serializers import ProductSerializer


class ProductListTestCase(APITestCase):
    url = reverse_lazy('product_view')

    def setUp(self):
        user = User.objects.create_user(username="test", password="test1234", email="test@check.com")
        access_token = str(SlidingToken.for_user(user))
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + access_token)
        self.product = Product.objects.create(id=2112123, created_at=now(), updated_at=now())

    def test_product_list_success(self):
        # get API response
        response = self.client.get(self.url, format='json')
        # get data from db
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        self.assertEqual(json.loads(response.content), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_product_list_method_not_allowed(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
