import asyncio
import csv

from django.core.management.base import BaseCommand

from apps.base.utils import background
from apps.product.models import Product


class Command(BaseCommand):
    """
        python manage.py upload_missing_data misc/sample.csv
    """
    help = 'Uploads a csv of products to Product table'
    can_import_settings = False

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.headers = []
        self.meta_data = {'command': 'upload_missing_data', 'total_rows': 0, 'fail_rows': 0, 'fail_data': []}
        self.data = []

    @background
    def _insert_row(self, row):
        self.meta_data['total_rows'] += 1
        try:
            product_obj_id = row.get('id')
            db_product_obj, created = Product.objects.update_or_create(
                id=product_obj_id,
                defaults=row,
            )
            self.data.append(row)
        except Exception as e:
            print(e)
            print('Failed to read row with error as {}'.format(str(e)))
            print(row)
            self.meta_data['fail_rows'] += 1
            self.meta_data['fail_data'].append({
                'id': row.get('id'),
                'error_message': str(e),
            }
            )

    async def _read_file(self, file_path):
        """
        Headers are in the following format.

        id, title, body_html, vendor, product_type,	created_at,	handle,	updated_at,	published_at, template_suffix, status, published_scope, tags, admin_graphql_api_id, image

        """
        with open(file_path, "r", encoding='utf-8-sig') as infile:
            reader = csv.DictReader(infile)
            headers = reader.fieldnames
            self.csv_index = {v: i for i, v in enumerate(headers)}
            for idx, row in enumerate(reader):
                if not row:
                    continue
                print(row)
                await self._insert_row(row)

    def add_arguments(self, parser):
        parser.add_argument(
            '-f', '--file_path', type=str,
            help='Specify the input csv file name'
        )

    def handle(self, *args, **options):
        asyncio.run(self._read_file(options.get('file_path')))
