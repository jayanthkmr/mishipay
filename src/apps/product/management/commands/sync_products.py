import shopify
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from apps.product.models import Product


class Command(BaseCommand):
    help = 'Syncs the Product Database'

    def init_process(self):
        with shopify.Session.temp(settings.SHOP_URL, settings.SHOPIFY_API_VERSION, settings.SHOPIFY_API_SECRET):
            products = shopify.Product.find()
            for each_product in products:
                try:
                    self.process_product(each_product)
                except CommandError as e:
                    print(each_product.attributes)
                    print('command error', e)

    @staticmethod
    def process_product(shopify_product_obj):
        product_obj_id = shopify_product_obj.get_id()
        product_obj_variants = shopify_product_obj.attributes['variants']
        del shopify_product_obj.attributes['variants']
        del shopify_product_obj.attributes['options']
        del shopify_product_obj.attributes['images']
        db_product_obj, created = Product.objects.update_or_create(
            id=product_obj_id,
            defaults=shopify_product_obj.attributes,
        )
        print(db_product_obj, created)

    def handle(self, *args, **options):
        self.init_process()
