from django.db import models

# Create your models here.

PUBLISHED_SCOPE_CHOICES_LIST = ['global', 'web']

PUBLISHED_SCOPE_CHOICES = [(e, e) for e in PUBLISHED_SCOPE_CHOICES_LIST]

PUBLISHED_SCOPE_CHOICES_MAP = {e: e for e in PUBLISHED_SCOPE_CHOICES_LIST}

STATUS_CHOICES_LIST = ['active', 'archived', 'draft']

STATUS_CHOICES = [(e, e) for e in STATUS_CHOICES_LIST]

STATUS_CHOICES_MAP = {e: e for e in STATUS_CHOICES}


class Product(models.Model):
    body_html = models.TextField(
        blank=True, null=True, default=None
    )

    created_at = models.DateTimeField()

    handle = models.CharField(max_length=255)

    id = models.PositiveBigIntegerField(primary_key=True)

    image = models.CharField(max_length=255, null=True, blank=True, default=None)

    images = models.JSONField(blank=True, null=True)

    options = models.JSONField(blank=True, null=True)

    product_type = models.CharField(max_length=100)

    published_at = models.DateTimeField(null=True)

    published_scope = models.CharField(choices=PUBLISHED_SCOPE_CHOICES, default='web', max_length=20)

    status = models.CharField(choices=STATUS_CHOICES, default='active', max_length=20)

    tags = models.CharField(max_length=64000, blank=True)

    template_suffix = models.CharField(max_length=255, null=True, blank=True, default=None)

    title = models.CharField(max_length=255)

    updated_at = models.DateTimeField()

    vendor = models.CharField(max_length=255)

    admin_graphql_api_id = models.CharField(max_length=255)


class ProductVariant(models.Model):
    barcode = models.CharField(max_length=20)

    compare_at_price = models.DecimalField(max_digits=10, decimal_places=2)

    created_at = models.DateTimeField()

    fulfillment_service = models.CharField(max_length=255)

    grams = models.IntegerField()

    id = models.PositiveBigIntegerField(primary_key=True)

    image_id = models.PositiveBigIntegerField()

    inventory_item_id = models.PositiveBigIntegerField()

    inventory_management = models.CharField(max_length=255)

    inventory_policy = models.IntegerChoices('InventoryPolicy', 'DENY CONTINUE')

    inventory_quantity = models.PositiveIntegerField()

    options = models.JSONField(blank=True, null=True)

    presentment_prices = models.JSONField(blank=True, null=True)

    position = models.PositiveSmallIntegerField()

    price = models.DecimalField(max_digits=10, decimal_places=2)

    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="variants")

    sku = models.CharField(max_length=255)

    taxable = models.BooleanField()

    title = models.CharField(max_length=255)

    updated_at = models.DateTimeField()

    weight = models.PositiveIntegerField()

    weight_unit = models.TextChoices('WeightUnit', 'g kg oz lb')
