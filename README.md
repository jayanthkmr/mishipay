# mishipay

This project is created using Django.
It contains the source code for mishipay assignment.

## Setup Instructions

First make sure that you have the following installed.

* Python 3 and virtualenv

Now do the following to setup project

```bash

cd mishipay

# one time
virtualenv -p $(which python3) pyenv

source pyenv/bin/activate

# one time or whenever any new package is added.
pip install -r requirements.txt

# generate a secret key or skip(has a default value) and
# then replace the value of `SECRET_KEY` in settings value

# update relevant variables in settings file

# run migrate
cd src
python manage.py migrate

```

We normally create a django superuser. You can also create it by running

```bash
python manage.py createsuperuser
```

### Testing Instructions

To run the test, you need to execute the following command.

```bash
python manage.py test
```

To generate the code coverage, execute following:

```bash
coverage run --source="." manage.py test
```

```bash
coverage report -m
```

The report is:
```text
Name                                      Stmts   Miss  Cover   Missing
-----------------------------------------------------------------------
apps/__init__.py                              0      0   100%
apps/base/__init__.py                         0      0   100%
apps/base/admin.py                            1      0   100%
apps/base/apps.py                             3      3     0%   1-5
apps/base/migrations/__init__.py              0      0   100%
apps/base/models.py                           1      0   100%
apps/base/tests.py                            0      0   100%
apps/base/urls.py                             3      0   100%
apps/base/utils.py                            6      6     0%   1-9
apps/base/views.py                            1      1     0%   1
apps/order/__init__.py                        0      0   100%
apps/order/admin.py                           3      0   100%
apps/order/apps.py                            3      3     0%   1-5
apps/order/migrations/0001_initial.py         5      0   100%
apps/order/migrations/__init__.py             0      0   100%
apps/order/models.py                         73      0   100%
apps/order/serializers.py                     6      0   100%
apps/order/tests.py                          24      0   100%
apps/order/urls.py                            4      0   100%
apps/order/views.py                          22      0   100%
apps/product/__init__.py                      0      0   100%
apps/product/admin.py                         4      0   100%
apps/product/apps.py                          3      3     0%   1-5
apps/product/migrations/0001_initial.py       6      0   100%
apps/product/migrations/__init__.py           0      0   100%
apps/product/models.py                       48      0   100%
apps/product/serializers.py                   6      0   100%
apps/product/tests.py                        26      0   100%
apps/product/urls.py                          3      0   100%
apps/product/views.py                        13      0   100%
core/__init__.py                              0      0   100%
core/asgi.py                                  4      4     0%   10-16
core/settings.py                             48      1    98%   31
core/urls.py                                  4      0   100%
core/wsgi.py                                  4      4     0%   10-16
manage.py                                    12      2    83%   12-13
-----------------------------------------------------------------------
TOTAL                                       336     27    92%
```

### Scheduling Instructions

Run this command to add all defined jobs from CRONJOBS to crontab (of the user which you are running this command with):
```bash
python manage.py crontab add
```

Show current active jobs of this project:

```bash
python manage.py crontab show
```

Removing all defined jobs is straight forward:

```bash
python manage.py crontab remove
```

### Serving Instructions

To access webserver, run the following command

```bash
cd src
python manage.py runserver
```

Now you can access the application at <http://localhost:8000> .

# Docker Instructions

To run the app, you need to perform only two steps.

### Build the image: 
  
This is done using the build command, which uses the Dockerfile you just created. To build the image, run the command 

```bash 
docker build . -t mishipay-v0.0
```
 

This command should be executed in the directory where the Docker file lives. The -t flag tags the image so that it can be referenced when you want to run the container.

### Run the image 

This is done using the docker run command. This will convert the built image into a running container

```bash 
docker run  -d -p 8000:8000 mishipay-v0.0
```

Now, the app is ready for use! View your app on the browser at <http://localhost:8000>.
